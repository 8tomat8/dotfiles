source $BYOBU_PREFIX/share/byobu/profiles/tmux
set -g mouse on
set -g pane-active-border-style bg=default,fg=colour7
set -g pane-border-style fg=colour8
