unbind-key -n F3
unbind-key -n F4
unbind-key -n F9

# Navigation
bind-key -n M-k display-panes \; select-pane -U
bind-key -n M-j display-panes \; select-pane -D
bind-key -n M-h display-panes \; select-pane -L
bind-key -n M-l display-panes \; select-pane -R
unbind -n C-h
unbind -n C-j
unbind -n C-k
unbind -n C-l
bind-key -n C-M-[ previous-window
bind-key -n C-M-] next-window
bind-key -n S-Up switch-client -p
bind-key -n S-Down switch-client -n

# Windows and panes
# bind-key -n C-t new-window -c "#{pane_current_path}" \; rename-window "-"
# bind-key -n C-o display-panes \; split-window -h -c "#{pane_current_path}"
# bind-key -n C-e display-panes \; split-window -v -c "#{pane_current_path}"
# bind-key -n C-w kill-pane
unbind -n C-x
bind-key -n C-Space resize-pane -Z
bind-key -n C-q kill-pane

# Clipboard
bind -T copy-mode-vi Enter send-keys -X copy-pipe-and-cancel "xclip -i -f -selection primary | xclip -i -selection clipboard"
set -g prefix F12
unbind-key -n C-a
