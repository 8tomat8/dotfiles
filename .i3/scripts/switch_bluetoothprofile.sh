MAC=$(pacmd list-cards | grep -o "[[:xdigit:]:]\{11,17\}" | sed 's/:/_/g')
if pacmd list-cards | grep -q "bluez_sink.$MAC.handsfree_head_unit" ; then
    pactl set-card-profile bluez_card.$MAC a2dp_sink
else
    pactl set-card-profile bluez_card.$MAC handsfree_head_unit
fi
