# DotFiles

## Updatesystem and install base packages
```bash
sudo pacman -Syu
sudo pacman -S git base-devel yay zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

## Pull dotfiles
```bash
# Generate new ssh key
ssh-keygen

# Add key to the gitlan MANUALLY!!!
git clone git@gitlab.com:8tomat8/dotfiles.git ./.dotfiles
alias dgit='git --git-dir ~/.dotfiles/.git --work-tree=$HOME'
dgit reset --hard
```

## Install standart packages
```bash
cat .pkglist.txt | egrep -v "(^#.*|^$)" | yay -S -
```

## Post install
```bash
# Add user to docker group
sudo groupadd docker
sudo usermod -aG docker $USER
```
